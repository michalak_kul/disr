import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;
import java.util.Optional;

public class EncodedFileManager implements FileManager {

    public void writeToFile(String text){
        byte[] encode = Base64.getEncoder().encode(text.getBytes(StandardCharsets.UTF_8));
        try {
            Files.write(Path.of("aa.txt"), encode);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Optional<String> readFromFile(){
        try {
            byte[] bytes = Files.readAllBytes(Path.of("aa.txt"));
            byte[] decode = Base64.getDecoder().decode(bytes);
            return Optional.of(new String(decode));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
