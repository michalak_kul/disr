public class HardcodedMessageProducer implements MessageProducer {
    @Override
    public String produceText() {
        return "HELLO";
    }
}
