import java.util.Optional;

public interface FileManager {
    void writeToFile(String text);
    Optional<String> readFromFile();
}
