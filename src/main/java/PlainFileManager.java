import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

public class PlainFileManager implements FileManager {
    @Override
    public void writeToFile(String text) {
        try {
            Files.write(Path.of("aa.txt"), List.of(text));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<String> readFromFile() {
        try {
            List<String> strings = Files.readAllLines(Path.of("aa.txt"));
            String reduce = strings.stream().reduce("", (s, s2) -> s + s2);
            return Optional.of(reduce);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
