import java.util.Scanner;

public class UserMessageProducer implements MessageProducer {

    @Override
    public String produceText(){
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
