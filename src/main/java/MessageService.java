public class MessageService {

    private final FileManager fileManager;
    private final MessageProducer messageProducer;

    public MessageService(FileManager fileManager, MessageProducer messageProducer) {
        this.fileManager = fileManager;
        this.messageProducer = messageProducer;
    }

    public void processMessage(){
        String text = messageProducer.produceText();
        fileManager.writeToFile(text);
        System.out.println(fileManager.readFromFile());
    }
}
