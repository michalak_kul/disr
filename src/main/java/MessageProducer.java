public interface MessageProducer {
    String produceText();
}
