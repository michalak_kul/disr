public class Main {
    public static void main(String[] args) {
        // 1. Napisac program ktory ma za zadanie pobrac od uzytkwnika tekst
        //  a nastepnie wyswietlic go wielkimi literami
        // 2. Zmodyfikowac program tak aby wyswietlil tekst zaszyty
        // w programie malymi literami.
        FileManager upperCaseMessagePrinter = new EncodedFileManager();
        MessageProducer userMessageProducer = new UserMessageProducer();
        FileManager lowerCaseMessagePrinter = new PlainFileManager();
        MessageProducer hardcodedMessageProducer = new HardcodedMessageProducer();
        MessageService messageService =
                new MessageService(upperCaseMessagePrinter, userMessageProducer);
//        MessageService messageService1 =
//                new MessageService(upperCaseMessagePrinter, hardcodedMessageProducer);
        messageService.processMessage();
//        messageService1.processMessage();

        //Zadanie
        //Napisz program który będzie działał w dwóch wersjach:
        //Będzie zapisywał sztywno zdefiniowany String do niekodowanego pliku
        //Będzie zapisywał plik z tekstem pobranym od użytkownika (konsola - Scanner),
        // który będzie zaszyfrowany za pomocą Base64 (Base64.getEncoder().encode())
        //W obu przypadkach wyświetl na koniec zawartość pliku!
    }
}
